## Knowledge Base
**Private portal project with a knowledge base module**

### Requirements

- Python 3.8 or higher.

### Installation:

1. git clone: `https://gitlab.com/tonnygram/knowledgebase.git`
2. Install the dependencies from file `requirements.txt`
3. Run command in terminal: `python manage.py runserver`
4. Go to site: `127.0.0.1:8000`

Admin Login:password `admin:admin` 
or
Test user Login:password `test:t12345678` (with limited permissions)

##### For clean installation:
1. git clone: `https://gitlab.com/tonnygram/knowledgebase.git`
2. Install the dependencies from file `requirements.txt`
3. Delete file `KnowledgeBase\db.sqlite3`
4. Change **SECRET_KEY** in file `KnowledgeBase\KnowledgeBase\settings.py`
5. Make settings for your database in the file `KnowledgeBase\KnowledgeBase\settings.py` in section **DATABASES**
6. Run command in terminal: `python manage.py makemigrations`
7. Run command in terminal: `python manage.py migrate`
8. Run command in terminal: `python manage.py createsuperuser` and input administrator login and password
9. Run command in terminal: `python manage.py runserver`

### Project:
1. `KnowledgeBase\core\` - module Knowledge Base
2. `KnowledgeBase\homepage\` - module home page
3. `KnowledgeBase\KnowledgeBase\` - core
4. `KnowledgeBase\static\` - folder with static files (*.css, *.js)
5. `KnowledgeBase\templates\` - folder with templates (*.html)

### Copyright

- Copyright (C) 2020 Feschenko Vladimir <<https://gitlab.com/tonnygram>>