from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls'), name='homepage'),
    path('knowledgebase/', include('core.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('mdeditor/', include('mdeditor.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
