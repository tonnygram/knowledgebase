from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import HomePage, DetailViewNote, SearchResultsView

app_name = 'core'
urlpatterns = [
    path('', HomePage.as_view(), name='index'),
    path('<int:pk>/', DetailViewNote.as_view(), name='detail'),
    path('search/', SearchResultsView.as_view(), name='result_search'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
