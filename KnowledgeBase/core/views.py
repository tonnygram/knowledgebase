from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render, redirect
from django.views.generic import DetailView, ListView
from django.views.generic.base import View

from .forms import MDEditorForm
from .models import City, Department, Category, Note

__all__ = {'HomePage', 'DetailViewNote', 'SearchResultsView'}


class HomePage(PermissionRequiredMixin, View):
    permission_required = ('core.view',)

    @staticmethod
    @login_required
    def get(request: HttpRequest) -> HttpResponse:
        cities = City.objects.all()
        departments = Department.objects.all()
        categories = Category.objects.all()
        notes = Note.objects.all()
        form = MDEditorForm
        context = dict(cities=cities, departments=departments, categories=categories, notes=notes, form=form)

        return render(request, 'core/index.html', context=context)

    def post(self, request: HttpRequest) -> HttpResponse:
        select_city = request.POST['select_city']
        select_department = request.POST['select_department']
        select_category = request.POST['select_category']
        if request.POST['input_title']:
            input_title = request.POST['input_title']
        else:
            messages.error(request, 'Требуется ввести заголовок!')
            return redirect('core:index')
        if request.POST['input_note']:
            input_note = request.POST['input_note']
        else:
            messages.error(request, 'Требуется ввести текст заметки!')
            return redirect('core:index')

        object_city = City.objects.get(name=select_city)
        object_department = Department.objects.get(name=select_department)
        object_category = Category.objects.get(title=select_category)

        if request.FILES:
            user_upload_file = request.FILES['user_upload_file']
            object_note = Note(city=object_city, department=object_department, category=object_category,
                               file=user_upload_file, title=input_title, content_markdown=input_note)
        else:
            object_note = Note(city=object_city, department=object_department, category=object_category,
                               title=input_title, content_markdown=input_note)
        object_note.save()

        messages.info(request, 'Ваша запись успешно добавлена в базу!')
        return redirect('core:index')


class DetailViewNote(PermissionRequiredMixin, DetailView):
    model = Note
    template_name = 'core/detail.html'
    permission_required = ('core.view',)


class SearchResultsView(PermissionRequiredMixin, ListView):
    model = Note
    template_name = 'core/result_search.html'
    permission_required = ('core.view',)

    @login_required
    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Note.objects.filter(
            Q(title__icontains=query) | Q(content__icontains=query)
        )
        return object_list
