from django.contrib import admin
from django.db import models
from mdeditor.widgets import MDEditorWidget

from .models import City, Department, Category, Note

__all__ = {'CityAdmin', 'DepartmentAdmin', 'CategoryAdmin', 'NoteAdmin'}

# admin:admin
# test:t12345678

class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_display_links = ('title',)
    search_fields = ('title',)


class NoteAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'content_markdown', 'file', 'created', 'city', 'department')
    list_display_links = ('title',)
    search_fields = ('title', 'content_markdown', 'file')

    formfield_overrides = {
        models.TextField: {'widget': MDEditorWidget}
    }


admin.site.register(City, CityAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Note, NoteAdmin)
