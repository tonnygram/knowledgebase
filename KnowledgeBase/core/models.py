import os

from django.db import models
from mdeditor.fields import MDTextField

__all__ = {'City', 'Department', 'Category', 'Note'}


class City(models.Model):
    name = models.CharField(max_length=25, db_index=True, help_text='Название города')

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"
        permissions = (('view', 'Право на просмотр'),)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100, db_index=True, help_text='Название отдела')

    class Meta:
        verbose_name = "Отдел"
        verbose_name_plural = "Отделы"
        permissions = (('view', 'Право на просмотр'),)

    def __str__(self):
        return self.name


class Category(models.Model):
    title = models.CharField(max_length=50, db_index=True, help_text='Имя категории')

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        permissions = (('view', 'Право на просмотр'),)

    def __str__(self):
        return self.title


class Note(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, help_text='Город')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, help_text='Отдел')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, help_text='Категория')
    title = models.CharField(max_length=100, db_index=True, help_text='Заголовок')
    file = models.FileField(upload_to='files', blank=True, help_text='Прикрепленный файл')
    created = models.DateField(auto_now_add=True, help_text='Дата создания заметки')
    content_markdown = MDTextField()

    class Meta:
        verbose_name = "Заметка"
        verbose_name_plural = "Заметки"
        permissions = (('view', 'Право на просмотр'),)

    def __str__(self):
        return self.title

    # def download_file(self):
    #     path_to_file = os.path.join('/homepage/portal/media/', self.file.name)
    #     return path_to_file.replace('/homepage/portal', '')
