from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import HomePage

app_name = 'homepage'
urlpatterns = [
    path('', HomePage.as_view(), name='index'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
