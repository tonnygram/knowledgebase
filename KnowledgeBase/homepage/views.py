__all__ = {'HomePage'}

from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.views.generic.base import View


class HomePage(View):
    @staticmethod
    def get(request: HttpRequest) -> HttpResponse:
        return render(request, 'homepage/index.html')
